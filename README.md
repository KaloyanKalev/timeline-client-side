
#How it works

![Alt](https://image.ibb.co/gkgwrp/Screenshot_from_2018_09_25_22_22_51.png)
![Alt](https://image.ibb.co/c09hWp/Screenshot_from_2018_09_25_22_22_43.png)
![Alt](https://image.ibb.co/iLr7xU/Screenshot_from_2018_09_25_22_22_27.png)
![Alt](https://image.ibb.co/eU8WP9/Screenshot_from_2018_09_25_22_22_18.png)

# Timeline

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
