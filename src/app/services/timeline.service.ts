import { Timeline } from './../models/Timeline';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Promise } from 'q';

@Injectable({
  providedIn: 'root'
})
export class TimelineService {
  readonly service_uri: string = 'http://127.0.0.1:1337/api/v1/';
  constructor(private _http: HttpClient) { }

  // Method which get all messages
  getTimeline() {
    return Promise(resolve => {
      this._http.get(`${this.service_uri}list_timeline`)
      .subscribe(res => resolve(<Timeline[]>res['data']));
    });
  }
  // Method which create post or comment
  setData(obj = {}, isPost = true) {
    if (isPost) {
      return this._http.post(`${this.service_uri}create_post/`, obj);
    } else {
      return this._http.post(`${this.service_uri}create_comment`, obj);
    }
  }
}
