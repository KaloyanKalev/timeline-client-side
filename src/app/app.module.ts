import { TimelineService } from './services/timeline.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AccordionModule, ModalModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { TimelineComponent } from './timeline/timeline.component';
import { CreateFormComponent } from './create-form/create-form.component';

@NgModule({
  declarations: [
    AppComponent,
    TimelineComponent,
    CreateFormComponent
  ],
  entryComponents: [CreateFormComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AccordionModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [TimelineService],
  bootstrap: [AppComponent]
})
export class AppModule { }
