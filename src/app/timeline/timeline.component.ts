import { CreateFormComponent } from './../create-form/create-form.component';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { TimelineService } from '../services/timeline.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  public timelineData;
  bsModalRef: BsModalRef;
  constructor(private _timelineService: TimelineService, private modalService: BsModalService) {}

  // Open modal service from other component and insert default state;
  openModal(id = 0, isMessage = true): void {
    const initialState = {
      title: isMessage ? 'Add New Message' : 'Add New Comment',
      type: isMessage ? 'Message' : 'Comment',
      isMessage: isMessage,
      postId: id
    };
    this.bsModalRef = this.modalService.show(CreateFormComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  // list all messages on page load
  async ngOnInit() {
    try {
      this.timelineData = await this._timelineService.getTimeline();
    } catch (e) {
      this.timelineData = [];
    }
  }

}
