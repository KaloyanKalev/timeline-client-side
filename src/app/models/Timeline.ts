interface Comment {
  username: string;
  comment: string;
  postId: string;
}

export interface Timeline {
  username: string;
  post: string;
  comments: Comment[];
}
