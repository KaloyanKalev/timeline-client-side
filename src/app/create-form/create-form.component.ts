import { TimelineService } from './../services/timeline.service';
import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.css']
})

// Using modal and form as a service component
export class CreateFormComponent {
  title: string;
  closeBtnName: string;
  type: string;
  isMessage: boolean;
  postId: string;
  text: string;
  username: string;

  constructor(public bsModalRef: BsModalRef, private _timelineService: TimelineService) {}

  // Submit form action
  submitForm(): void {
    if (this.isMessage) {
     this.submitAction({username: this.username, post: this.text});
    } else {
     this.submitAction({username: this.username, comment: this.text, postId: this.postId});
    }
  }

  // When submit form check status close modal and reload page.
  submitAction(obj = {}): void {
    this._timelineService.setData(obj, this.isMessage)
    .subscribe(res => {
      if (res['status'] === 200) {
        this.bsModalRef.hide();
        location.reload();
      }
    });
  }
}
